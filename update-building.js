const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')
async function main () {
  const newInformaticsBuilding = await Building.findById('621a7cf344a5f9dfef46328c')
  const room = await Room.findById('621a7cf344a5f9dfef463291')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}
main().then(() => {
  console.log('Finish')
})
